import React from 'react';
import M from 'materialize-css';
import './App.css';
import axios from "axios";

export default class App extends React.Component {
  constructor(props){
    super(props);
      this.state = {
        entrada:'',
        saida:'',
        horarioDiurno:'',
        horarioNoturno:'',
      }
    }

    componentDidMount(){
      M.AutoInit();

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.timepicker');
        var instances = M.Timepicker.init(elems);
      });
    }
    handleChange = event =>
    {
      switch (event.target.id) {
        case 'entrada': 
        this.setState(
          {
              entrada : event.target.value
          }
        )
        break;
        case 'saida': 
          this.setState(
            {
              saida : event.target.value 
            } 
          )
  
        default:
          break;
      }
    }

    handleSubmit = event => {
      event.preventDefault();
 console.log("entrada:", this.state.entrada)
      const periodo = {
        entrada: this.state.entrada,
        saida: this.state.saida
      };

      axios.post(`http://255.255.255.0:3000/turno`, periodo)
        .then(res => {
          console.log(res);
          console.log(res.data);
          this.setState(
            {
              horarioDiurno:res.data.horarioDiurno,
              horarioNoturno:res.data.horarioNoturno
            }) 
        })
    }

  render(){
    return (
      <div className="App">
        <div className='container'>
          <div className='row'>
            <div className ='col s12 m6'>
              <h1>Período de Trabalho</h1>
              <form onSubmit={this.handleSubmit}>
                <div className='input-field'>
                  <input type="text" id='entrada' className="timepicker" onChange={this.handleChange}/>
                  <label for='time'>Horario de entrada</label>
                </div>
                <div className='input-field'>
                  <input type="text" id='saida' className="timepicker" onChange={this.handleChange}/>
                  <label for='time'>Horario de Saída</label>
                </div>
                <button className="waves-effect waves-light btn" type="submit">Verificar Horas</button>
              </form>
              <div className="row">
                <div className=" col s12 m6" >
                  <div className="card yellow darken-1">
                    <div className="card-content black-text">
                      <p>Horas Trabalhadas no período Diurno</p>
                      <span className="card-title">08:00</span>
                    </div>
                  </div>
                </div>
                <div className="col s12 m6">
                  <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                      <p>Horas Trabalhadas no período Noturno</p>
                      <span className="card-title">01:00</span>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
}
}


