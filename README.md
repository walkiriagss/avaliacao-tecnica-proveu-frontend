# Clone este repositório
$ git clone <https://gitlab.com/walkiriagss/avaliacao-tecnica-proveu-frontend.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd periodo-trabalho

# Instale as dependências
$ npm install

# Execute a aplicação em modo de desenvolvimento
$ npm start

# O servidor inciará na porta:3000 - acesse <http://localhost:3000> 
